# orange

1) Grupa - aplikacja Vue JS 
* Zbudowanie obrazu dockerowego
* Wdrożenie na platformę Kubernetes
* Udostępnienie aplikacji poprzez NodePort
https://github.com/gothinkster/vue-realworld-example-app

### Dodatkowo:
* Przekazanie do ENV zmiennej: ``` ENV_ORANGE_GROUP=1 ```

2) Grupa - wdrożenie ElasticSearch
* Zbudowanie obrazu dockerowego z Stempel Polish Analysis
* 1 master, 2 slave
* Zbudowanie LoadBalancera dla ElasticSearch

### Dodatkowo:
* Przekazanie do ENV zmiennej: ``` ENV_ORANGE_GROUP=2 ```

3) Grupa - aplikacja Rev Proxy
* Użycie Varnisha
* Użycie nginxa
* https://github.com/cosmicjs/static-website

### Dodatkowo:
* Przekazanie do ENV zmiennej: ``` ENV_ORANGE_GROUP=3 ```

4) Grupa - Jenkins X
5) Grupa - Calico


# HELM

1) MongoDB HA Cluster
2) PostgreSQL HA Cluster
3) RabbitMQ HA Cluster
